#include "Graphics_Console.h"
#include <conio.h> // Necesario para HANDLE
#include <time.h> // Necesario para srand((unsigned)time(NULL))
#include <stdio.h>
#include <stdlib.h>

#define INICI_X 4	//punto inicial en el plano 
#define INICI_Y 5
#define FI_X 36
#define FI_Y 25
#define MUR 219
#define PILOTA 64	
#define OFFSET 2 //mida de la raqueta desde la mitad hasta un extremo#pragma once
#define COL_X 30

void joc();
void iniciarMur(int &mur);
void Taulell(HANDLE hScreen); // Pinta vidas, nivel, tiempo y puntos//n